<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TOP
Route::get('/', 'HomeController@index');
Route::get('/search', 'HomeController@search');

// animal
Route::get('/animal/{id}', 'AnimalController@detail');

// breeder
Route::get('/breeder/{id}', 'BreederController@detail');
