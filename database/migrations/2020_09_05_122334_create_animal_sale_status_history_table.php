<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalSaleStatusHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animal_sale_status_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('animal_id');
            $table->string('sale_status');
            $table->timestamps();

            $table->foreign('animal_id')
                  ->references('animal_id')->on('animals')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animal_sale_status_histories');
    }
}
