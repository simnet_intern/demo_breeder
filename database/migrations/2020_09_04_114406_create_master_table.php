<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * 支払い方法のマスタテーブル
         */
        Schema::create('payment_types', function (Blueprint $table) {
            $table->increments('payment_type_id');
            $table->string('payment_type_name');
            $table->timestamps();
        });

        /**
         * 販売情報のマスタテーブル
         */
        Schema::create('sale_statuses', function (Blueprint $table) {
            $table->increments('sale_status_id');
            $table->string('sale_status_name');
            $table->timestamps();
        });

        /**
         * 犬種マスタテーブル
         */
        Schema::create('dog_kinds', function (Blueprint $table) {
            $table->string('dog_kind_code')->primary();
            $table->string('dog_kind_name');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_types');
        Schema::dropIfExists('sale_statuses');
        Schema::dropIfExists('dog_kinds');
    }
}
