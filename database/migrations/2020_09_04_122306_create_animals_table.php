<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * 動物テーブル
         */
        Schema::create('animals', function (Blueprint $table) {
            $table->string('animal_id')->primary();
            $table->string('animal_type');
            $table->string('kind_cd');
            $table->string('price');
            $table->string('comment');
            $table->string('birthday');
            $table->string('sex');
            $table->string('breeder_id');
            $table->integer('image_number');
            $table->timestamp('publication_date');
            $table->timestamps();

            $table->foreign('breeder_id')
                   ->references('breeder_id')->on('breeders')
                   ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
        Schema::dropIfExists('breeders');
    }
}
