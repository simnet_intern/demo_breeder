<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBreedersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * ブリーダーテーブル
         */
        Schema::create('breeders', function (Blueprint $table) {
            $table->string('breeder_id')->primary();
            $table->string('last_name');
            $table->string('first_name');
            $table->string('last_name_kana');
            $table->string('first_name_kana');
            $table->string('password');
            $table->string('postcode');
            $table->string('prefectures');
            $table->string('cities');
            $table->string('block');
            $table->string('building');
            $table->string('phoneNumber');
            $table->string('email');
            $table->string('biography');
            $table->unsignedInteger('payment_type_code');
            $table->timestamps();

            $table->foreign('payment_type_code')
                   ->references('payment_type_id')->on('payment_types')
                   ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('breeders');
        Schema::dropIfExists('payment_types');
    }
}
