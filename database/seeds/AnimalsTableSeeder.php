<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnimalsTableSeeder extends Seeder
{
    private const DOG_KINDS = [
        'toy-poodle',
        'chihuahua',
        'shiba',
        'long-chihuahua',
        'tiny-poodle',
        'french-bulldog',
        'mame-shiba',
        'pomeranian',
        'miniature-dachshund',
        'teacup-poodle',
    ];

    private const BREEDER_IDS = [
        '3tihho9vhoa',
        'hhahrrb',
        'bshsdnda',
        'qfegagag',
        'bahboainbe',
        'banobneia',
        'beniobnaoi',
        '489hg9h03',
        'ehg0hhhhhhhhh3',
    ];

    private const INSERT_ANIMAL_DATA_COUNTS = 300;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ja_JP');

        $tmpDay = $faker->dateTimeBetween('+1day', '+1year');

        for($i = 0; $i < self::INSERT_ANIMAL_DATA_COUNTS; $i++) {
            DB::table('animals')->insert([
                'animal_id'         => '2020_' . $i,
                'animal_type'       => 'dog',
                'kind_cd'           => $faker->randomElement(self::DOG_KINDS),
                'price'             => $faker->numberBetween(150000, 300000),
                'comment'           => $faker->realText(40),
                'birthday'          => $tmpDay->format('Y-m-d'),
                'sex'               => $faker->randomElement([1,0]),
                'breeder_id'        => $faker->randomElement(self::BREEDER_IDS),
                'publication_date'  => $tmpDay->modify('+' . random_int(0, 30) . 'day')->format('Y-m-d'),
                'image_number'      => $faker->numberBetween(0, 11),
                'created_at'        => now(),
                'updated_at'        => now(),
            ]);
        }
    }
}
