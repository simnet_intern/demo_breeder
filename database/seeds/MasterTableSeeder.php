<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * 支払い方法マスタ
         */
        DB::table('payment_types')->insert([
            ['payment_type_name' => '現金'],
            ['payment_type_name' => 'カード'],
            ['payment_type_name' => '電子決済'],
        ]);

        /**
         * 販売状態マスタ
         */
        DB::table('sale_statuses')->insert([
            ['sale_status_name' => '販売中'],
            ['sale_status_name' => '成約済み'],
            ['sale_status_name' => '準備中'],
            ['sale_status_name' => '削除済み'],
        ]);

        /**
         * 犬種マスタ
         */
        DB::table('dog_kinds')->insert([
            ['dog_kind_code' => 'toy-poodle'         , 'dog_kind_name' => 'トイプードル'],
            ['dog_kind_code' => 'chihuahua'          , 'dog_kind_name' => 'チワワ'],
            ['dog_kind_code' => 'shiba'              , 'dog_kind_name' => '柴犬'],
            ['dog_kind_code' => 'long-chihuahua'     , 'dog_kind_name' => 'ロングコートチワワ'],
            ['dog_kind_code' => 'tiny-poodle'        , 'dog_kind_name' => 'タイニープードル'],
            ['dog_kind_code' => 'french-bulldog'     , 'dog_kind_name' => 'フレンチブルドッグ'],
            ['dog_kind_code' => 'mame-shiba'         , 'dog_kind_name' => '豆柴'],
            ['dog_kind_code' => 'pomeranian'         , 'dog_kind_name' => 'ポメラニアン'],
            ['dog_kind_code' => 'miniature-dachshund', 'dog_kind_name' => 'ミニチュアダックスフンド'],
            ['dog_kind_code' => 'teacup-poodle'      , 'dog_kind_name' => 'ティーカッププードル'],
        ]);
    }
}
