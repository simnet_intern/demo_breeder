<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MasterTableSeeder::class,   // マスター関連
            BreedersTableSeeder::class, // ブリーダー関連
            AnimalsTableSeeder::class,  // 動物関連
        ]);
    }
}
