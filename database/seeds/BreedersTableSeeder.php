<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BreedersTableSeeder extends Seeder
{
    private const BREEDER_IDS = [
        '3tihho9vhoa',
        'hhahrrb',
        'bshsdnda',
        'qfegagag',
        'bahboainbe',
        'banobneia',
        'beniobnaoi',
        '489hg9h03',
        'ehg0hhhhhhhhh3',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ja_JP');

        foreach(self::BREEDER_IDS as $id) {
            DB::table('breeders')->insert([
                'breeder_id'        => $id,
                'last_name'         => $faker->lastName(),
                'first_name'        => $faker->firstName(),
                'last_name_kana'    => '',
                'first_name_kana'   => '',
                'password'          => Hash::make('testtest'),
                'postcode'          => $faker->postcode(),
                'prefectures'       => $faker->prefecture(),
                'cities'            => $faker->city(),
                'block'             => $faker->streetAddress(),
                'building'          => $faker->secondaryAddress(),
                'phoneNumber'       => $faker->phoneNumber(),
                'email'             => $faker->email(),
                'biography'         => $faker->realText(200),
                'payment_type_code' => $faker->randomElement([1, 2, 3]),
                'created_at'        => now(),
                'updated_at'        => now(),
            ]);
        }
    }
}
