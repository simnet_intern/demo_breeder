@extends('layouts.app')

@section('content')

<div class="uk-container uk-margin">
    <h3>子犬詳細 : {{ $animal['animal_id'] }}</h3>

    <div class="uk-container uk-container-xsmall">
        <table class="uk-table uk-table-striped uk-margin">
            <tbody>
            <tr>
                <td>犬種</td>
                <td>{{ $animal['dog_kind_name'] }}</td>
                <td>地域</td>
                <td>{{ $animal['prefectures'] }}{{ $animal['cities'] }}</td>
            </tr>
            <tr>
                <td>価格</td>
                <td>{{ $animal['price'] }} 円</td>
                <td>誕生日</td>
                <td>{{ $animal['birthday'] }}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="uk-align-center uk-container-small">
        <img class="uk-align-center" src="../img/dog_{{ $animal['image_number'] }}.jpg" alt="dog" width="70%" height="70%">
        <div>
            <span>ブリーダー情報</span>
            <a href="{{url('/breeder')}}/{{ $animal['breeder_id'] }}">
                <span>{{ $animal['last_name'] }} {{ $animal['first_name'] }}</span>
            </a>
        </div>
        <div class="uk-margin">
            <span>子犬説明</span>
            <span>
                {{ $animal['biography'] }}
            </span>
        </div>
        <button class="uk-button uk-button-default uk-width-1-1 uk-margin-small-bottom">お問合わせボタン</button>
    </div>

</div>

@endsection
