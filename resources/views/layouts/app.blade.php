<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    {{-- Styles --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{--Import Google Icon Font--}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
    integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.0.min.js"></script>

    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="//jpostal-1006.appspot.com/jquery.jpostal.js"></script>

    {{-- UIkit CSS --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css" />

    {{-- UIkit JS --}}
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
</head>

<body>
    <header>
    <nav>
        <div class="uk-container">
            <a href="{{ url('/') }}" style="text-decoration: none;"><h1 class='uk-heading-primary'>デモサイト</h1></a>
        </div>
    </nav>
    </header>

  @yield('content')

  <footer>
    <div class="uk-container uk-container-center">
      <div class="container center-align">
        © 2020 Copyright demo_breeder
      </div>
    </div>
  </footer>

  @yield('script')
</body>

</html>

@yield('style')
<style>
</style>
