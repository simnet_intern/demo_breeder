@extends('layouts.app')

@section('content')

<div class="uk-container uk-margin">
    <span>このサイトはデモサイトです。</span>

    <div class="uk-margin">
        <span>検索</span>
        <form class="uk-flex">
            <div>
                <span>犬種</span>
                <select class="uk-select">
                    <option>犬</option>
                    <option>猫</option>
                </select>
            </div>
            <div class="uk-margin-left">
                <span>地域</span>
                <select class="uk-select">
                    <option>宮城</option>
                    <option>仙台</option>
                </select>
            </div>
        </form>
    </div>

    <div>
        <span>一覧</span>
        <div class="uk-child-width-1-3 uk-text-center" uk-grid>
            <div>
                <div class="uk-card uk-card-default">
                    <div>Item</div>
                    <div>子犬</div>
                    <a href="{{ url("/animal" )}}">
                        <img src="img/sample.jpg" alt="dog" width="70%" height="70%">
                    </a>
                    <div>
                        説明です。説明です。
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card uk-card-default">
                    <div>Item</div>
                    <div>子犬</div>
                    <img src="img/sample.jpg" alt="dog" width="70%" height="70%">
                    <div>
                        説明です。説明です。
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card uk-card-default">
                    <div>Item</div>
                    <div>子犬</div>
                    <img src="img/sample.jpg" alt="dog" width="70%" height="70%">
                    <div>
                        説明です。説明です。
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-card uk-card-default">
                    <div>Item</div>
                    <div>子犬</div>
                    <img src="img/sample.jpg" alt="dog" width="70%" height="70%">
                    <div>
                        説明です。説明です。
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
