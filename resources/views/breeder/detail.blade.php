@extends('layouts.app')

@section('content')

<div class="uk-container uk-margin">
    <h3>ブリーダー詳細</h3>

    <div class="uk-margin">
        <div>
            <span>名前</span>
            <span>{{ $breeder['last_name'] }} {{ $breeder['first_name'] }}</span>
        </div>
        <div>
            <span>地域</span>
            <span>{{ $breeder['prefectures'] }}{{ $breeder['cities'] }}</span>
        </div>
    </div>

    <div class="uk-align-center uk-container-small">
        <img class="uk-align-center" src="../img/sample_breeder.png" alt="dog" width="70%" height="70%">
        <div class="uk-margin">
            <span>紹介</span>
            <span>
                {{ $breeder['biography'] }}
            </span>
        </div>
    </div>

    <div class="uk-margin">
        <span>掲載一覧</span>
        <div class="uk-child-width-1-5 uk-text-center" uk-grid>
            @foreach($animalList as $animalValues)
                <div>
                    <div class="uk-card uk-card-default">
                        <div>{{$animalValues['animal_id']}}</div>
                        <a href="{{ url('/animal') }}/{{$animalValues['animal_id']}}">
                            <img src="../img/dog_{{ $animalValues['image_number'] }}.jpg" alt="dog" width="70%" height="70%">
                        </a>
                        @if($animalValues['animal_type'] === 'dog')
                            <div>子犬</div>
                        @else
                            <div>子猫</div>
                        @endif
                        <div>{{$animalValues['dog_kind_name']}}</div>
                        @if($animalValues['sex'] === '0')
                            <img src="../img/male.png" alt="male" width="20px" height="20px">
                        @elseif($animalValues['sex'] === '1')
                            <img src="../img/female.png" alt="female" width="20px" height="20px">
                        @endif
                        <div>
                            {{$animalValues['comment']}}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

</div>

@endsection
