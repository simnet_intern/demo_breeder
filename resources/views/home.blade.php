@extends('layouts.app')

@section('content')

<div class="uk-container uk-margin">
    <span>このサイトはデモサイトです。</span>

    {{-- 検索フォームエリア --}}
    <div class="uk-margin">
        <span>検索</span>
        <form action='{{ url('/search/') }}' method="get">
            <div class="uk-flex uk-flex-bottom">
                <div class="uk-width-1-4@s">
                    <label for="kind">犬種</label>
                    <select id="kind" name="anima_kind" class="uk-select">
                        <option value="">選択してください</option>
                        @foreach($animalKindList as $animalKind)
                            <option
                                value="{{ $animalKind['dog_kind_code'] }}"
                                @if($animalKind['dog_kind_code'] === $anima_kind) selected @endif
                            >{{ $animalKind['dog_kind_name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="uk-width-1-4@s uk-margin-left">
                    <label for="area">地域</label>
                    <select id="area" name="area_id" class="uk-select">
                        <option value="">選択してください</option>
                        @foreach (config('const.PREF_LIST') as $prefId => $name)
                            <option
                                value="{{$prefId}}"
                                @if((string)$prefId === $area_id) selected @endif
                            >{{$name}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="uk-button uk-button-default uk-margin-left" style="height: 50%">検索</button>
            </div>
        </form>
    </div>

    {{-- 一覧表示エリア --}}
    <div class="uk-margin">
        <span>一覧</span>
        <div class="uk-child-width-1-3 uk-text-center" uk-grid>
            @foreach($animalList as $animalValues)
            <div>
                {{-- 一匹毎のcard --}}
                <div class="uk-card uk-card-default">
                    <div>{{$animalValues['animal_id']}}</div>
                    <a href="{{url('/animal')}}/{{$animalValues['animal_id']}}">
                        <img src="img/dog_{{ $animalValues['image_number'] }}.jpg" alt="dog" width="70%" height="70%">
                    </a>
                    <div>
                        <span>{{$animalValues['dog_kind_name']}}</span>
                        @if($animalValues['sex'] === '0')
                            <img src="img/male.png" alt="male" width="20px" height="20px">
                        @elseif($animalValues['sex'] === '1')
                            <img src="img/female.png" alt="female" width="20px" height="20px">
                        @endif
                        <div>
                    </div>
                        <span>{{ $animalValues['prefectures'] }}{{ $animalValues['cities'] }}</span>
                    </div>
                    <div>
                        <span>{{$animalValues['comment']}}</span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    {{-- pager --}}
    <div class="uk-margin">
        @if($area_id || $anima_kind)
            {{ $animalList->appends(request()->query())->links('vendor.pagination.default') }}
        @else
            {{ $animalList->links('vendor.pagination.default') }}
        @endif
    </div>

</div>

@endsection
