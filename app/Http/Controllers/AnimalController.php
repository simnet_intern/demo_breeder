<?php

namespace App\Http\Controllers;

use App\Animal;
use Illuminate\Http\Request;

class AnimalController extends Controller
{
    /**
     * 動物詳細
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function detail(Request $request)
    {
        $dogId = $request->id;

        if (!$dogId) {
            return abort(404);
        }

        $animalValues = Animal::leftJoin('dog_kinds', 'dog_kinds.dog_kind_code', '=', 'animals.kind_cd')
            ->leftJoin('breeders', 'breeders.breeder_id', '=', 'animals.breeder_id')
            ->where('animal_id', $dogId)
            ->first();

        // nullの状態で toArray するとエラーになってしまうのでチェックする
        if (empty($animalValues)) {
            return abort(404);
        }

        $animalValues->toArray();

        return view('animal/detail', [
            'animal' => $animalValues
        ]);
    }
}
