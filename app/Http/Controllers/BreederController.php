<?php

namespace App\Http\Controllers;

use App\Animal;
use App\Breeder;
use Illuminate\Http\Request;

class BreederController extends Controller
{
    /**
     * ブリーダー詳細
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function detail(Request $request)
    {
        $breederId = $request->id;

        if (!$breederId) {
            return abort(404);
        }

        $breederValues = Breeder::where('breeder_id', $breederId)->first();
        // nullの状態で toArray するとエラーになってしまうのでチェックする
        if (empty($breederValues)) {
            return abort(404);
        }

        $breederValues->toArray();

        // ブリーダーに紐づく動物を取得
        $animalList = Animal::leftJoin('dog_kinds', 'dog_kinds.dog_kind_code', '=', 'animals.kind_cd')
                            ->where('breeder_id', $breederId)
                            ->take(15)
                            ->get()
                            ->toArray();

        return view('breeder/detail', [
            'breeder'     => $breederValues,
            'animalList'  => $animalList,
        ]);
    }
}
