<?php

namespace App\Http\Controllers;

use App\Animal;
use App\DogKind;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * 初期ページを表示
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $animalList = Animal::leftJoin('dog_kinds','dog_kinds.dog_kind_code','=','animals.kind_cd')
                        ->leftJoin('breeders', 'breeders.breeder_id', '=', 'animals.breeder_id')
                        ->paginate(15);

        return view('home', [
            'animalList'     => $animalList,
            'animalKindList' => self::getDogKindList(),
            'anima_kind'     => '',
            'area_id'        => '',
        ]);
    }

    /**
     * 条件で絞り込んだ一覧ページを返す
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $base = Animal::leftJoin('dog_kinds','dog_kinds.dog_kind_code','=','animals.kind_cd')
                    ->leftJoin('breeders', 'breeders.breeder_id', '=', 'animals.breeder_id');

        if ($request->anima_kind) {
            $base->where('kind_cd', $request->anima_kind);
        }

        if ($request->area_id) {
            $areaName = config('const.PREF_LIST')[$request->area_id];
            $base->where('prefectures', $areaName);
        }

        $animalList = $base->paginate(15);

        return view('home', [
            'animalList'     => $animalList,
            'animalKindList' => self::getDogKindList(),
            'anima_kind'     => $request->anima_kind,
            'area_id'        => $request->area_id,
        ]);
    }

    /**
     * 犬種一覧を返す
     */
    private static function getDogKindList(): array
    {
        return DogKind::all()->toArray();
    }
}
